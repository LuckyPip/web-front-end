
import {ChipList} from "./ChipList";

export interface DragDropListItem {

  name:string;
  label:string;
  chipList?:ChipList

}
