export interface FireArrayField<T> {
  type:string
  values:T[]
}
