import {
  AngularFirestore,
  AngularFirestoreCollection, DocumentChangeAction, DocumentData,
  DocumentReference, DocumentSnapshot,
  QueryFn
} from "@angular/fire/firestore";
import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";

@Injectable()
export abstract class CollectionService<T> {


  protected constructor(protected collection: string, protected fireStore: AngularFirestore) {

  }

  public add(doc: T): Promise<DocumentReference> {
    return this.fireStore.collection(this.collection).add(doc);
  }

  public update(id, doc: T): Promise<void> {
    return this.fireStore.doc(this.collection + "/" + id).update(doc);
  }


  /**
   * get the collection reference of given collection name.
   * @param queryFn
   */
  public get(documentId: string, queryFn?: QueryFn): Observable<T> {

    return Observable.create(observer => {
      this.fireStore.collection<T>(this.collection, queryFn).doc(documentId).snapshotChanges().subscribe(
        actions => {
          observer.next(this.getDocumentData<T>(actions.payload));
          observer.complete();
        });
    });
  }


  private getDocumentData<T>(payload: DocumentSnapshot<any>): T {
    const data: T = payload.data() as T;
    data['id'] = payload.id as string;
    //  this.lastObj = data
    //return this.lastObj;
    return data
  }


  /**
   * get data from document change action.
   * @param action
   */
  private getPayload<T>(action: DocumentChangeAction<DocumentData>): T {
    if (action.payload != null && action.payload != undefined) {
      const data: T = action.payload.doc.data() as T;
      data['id'] = action.payload.doc.id as string;
      //  this.lastObj = data
      //return this.lastObj;
      return data
    } else {
      let u1: T = null;
      return u1;
    }
  }

  /**
   * get a sumb collection of main collection
   * @param subCollectionPath
   * @param documentId
   * @param queryFn
   */
  public querySubCollection(queryFn: QueryFn, ...subCollectionPaths: SubCollectionInfo[]): Subject<T[]> {
    let collection: AngularFirestoreCollection = null;

    let dataSubject: Subject<T[]> = new Subject<T[]>();

    if (subCollectionPaths.length > 0) {
      for (let i = 0; i < subCollectionPaths.length; i++) {
        if (collection) {
          collection = this.getSubCollection(collection, subCollectionPaths[i], queryFn)
        } else {
          collection = this.getSubCollection(this.fireStore.collection(this.collection), subCollectionPaths[i], queryFn)
        }
      }

    } else {
      collection = this.fireStore.collection(this.collection, queryFn);
    }

    collection.snapshotChanges().subscribe(
      actions => {
        dataSubject.next(actions.map(a => {
          return this.getPayload<T>(a)
        }));
      });


    return dataSubject;
  }

  /**
   * get a sub collection from a given collection
   * @param collection
   * @param subCollection sub collection info along with document id
   */
  private getSubCollection(collection: AngularFirestoreCollection, subCollection: SubCollectionInfo, queryFn?: QueryFn): AngularFirestoreCollection {
    return collection.doc(subCollection.documentId).collection(subCollection.subCollection, queryFn)
  }

  /**
   * Return all documents in the collection
   * @returns {Observable<T[]>}
   */
  public getAll(): Observable<T[]> {
    return this.fireStore.collection<T>(this.collection).valueChanges();
  }

  public setDoc(id, doc: T): Promise<void> {
    return this.fireStore.collection(this.collection).doc(id).set(doc);
  }
}

/**
 * sub collection info used for sub collection retrieval
 */
export interface SubCollectionInfo {
  documentId: string,
  subCollection: string;
}
