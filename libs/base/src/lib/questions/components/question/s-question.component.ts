import {Component, OnInit} from '@angular/core';
import {ChipList, Question} from "@satipasala/base";
import {FireArrayField} from "../../../core-components/models/fields/FireArrayField";
import {Validator} from "@angular/forms";

@Component({
  selector: 's-question',
  templateUrl: './s-question.component.html',
  styleUrls: ['./s-question.component.scss']
})
export class SQuestionComponent implements OnInit {

  question: Question<any> = <Question<any>>{}

  constructor() {
  }

  ngOnInit() {
  }

  //appearance change based on question type
  setQuestionType() {

  }

}

