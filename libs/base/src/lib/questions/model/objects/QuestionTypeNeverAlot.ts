import {Option} from "@satipasala/base";
import {QuestionType} from "../Question";

export class QuestionTypeNeverAlot implements QuestionType<Number> {
  id:"3";
  name: "Multiple Choice";
  type:"MULTIPLE_CHOICE";
  answerType:"NEVER_ALOT";
  fieldType:string = "radio";
  label = "Answer Never A lot";


  constructor() {

  }

}
