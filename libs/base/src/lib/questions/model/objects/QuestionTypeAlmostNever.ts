import {Option} from "@satipasala/base";
import {QuestionType} from "../Question";

export class QuestionTypeAlmostNever implements QuestionType<Number> {
  id:"1";
  name: "Multiple Choice";
  type:"MULTIPLE_CHOICE";
  answerType:"ALMOST_NEVER";
  fieldType: "radio";
  label = "Answer Almost Never";

  constructor() {


  }

}
