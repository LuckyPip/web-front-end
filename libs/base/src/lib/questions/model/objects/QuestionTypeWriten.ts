import {Option, Question} from "@satipasala/base";
import {QuestionLevel, QuestionType, ScoringMechanism} from "../Question";
import {Label} from "@satipasala/model";

export class QuestionTypeWriten implements QuestionType<String>  {

  id:"4";
  name: "Written";
  type:"WRITTEN";
  answerType:"WRITTEN";
  fieldType:string =  "text";
  label = "Answer Written";
  constructor() {
  }
}
