import { Option} from "@satipasala/base";
import {QuestionType} from "../Question";

export class QuestionTypeRarelySometimes implements QuestionType<Number> {
  id:"2";
  name: "Multiple Choice";
  type:"MULTIPLE_CHOICE";
  answerType:"RARELY_SOMETIMES";
  fieldType: "radio";
  label = "Answer Rarely Some Times";

  constructor() {


  }

}
