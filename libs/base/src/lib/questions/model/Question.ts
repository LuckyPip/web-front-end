
import {Label} from "@satipasala/model"; ;
import {Chip, FormField, Option} from "@satipasala/base";

export interface Question<VALUE_TYPE> extends FormField<VALUE_TYPE>{
  id:string;
  description?:string
  questionType:QuestionType<VALUE_TYPE>;
  labels:Label[];
  scoringMechanism:ScoringMechanism
  questionLevel:QuestionLevel;
}


export interface QuestionType<T> {
  id:string
  type:string
  name:string
  label:string;
  fieldType:string;
  answerType:string;
}


export interface QuestionLevel extends Chip{

}


export interface ScoringMechanism {
  id: string;
  name: string;
  description: string;
  type: "SCALE" | "REVERSE_SCALE"
}
