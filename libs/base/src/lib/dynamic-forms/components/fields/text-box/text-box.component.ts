import {ChangeDetectorRef, Component, Input, OnInit, Optional, Self} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {ErrorStateMatcherFactory, FormField} from "@satipasala/base";
import {MatFormFieldControl} from "@angular/material";
import {AbstractFieldComponent} from "../AbstractFieldComponent";

@Component({
  selector: 'satipasala-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: TextBoxComponent}],
})
export class TextBoxComponent extends AbstractFieldComponent<FormField<any>>{
  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    super(errorStateMatcherFactory, cdRef)

  }
}
