import {AfterViewChecked, ChangeDetectorRef, Input, OnInit} from "@angular/core";
import {ErrorStateMatcherFactory, FormField} from "@satipasala/base";
import {FormGroup} from "@angular/forms";

export class AbstractFieldComponent<T extends FormField<any>> implements OnInit,AfterViewChecked{
  get field(): T {
    return this._field;
  }
  @Input()
  set field(value: T) {
    this._field = value;
  }
  private _field: T;

   @Input() form: FormGroup;
  matcher:any;
  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    this.matcher = this.errorStateMatcherFactory.getErrorStateMatcher('errorStateMatcher');

  }

  ngOnInit() {

  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  get isValid() {
    return this.form.controls[this._field.name].valid;
  }

  get isDirty() {
    return this.form.controls[this._field.name].dirty;
  }
}
