export interface Label {
  type: string;
  category: string;
  name: string;
}
