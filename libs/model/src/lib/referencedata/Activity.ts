import {ActivityType} from "./ActivityType";
import {RefData} from "./RefData";

export class Activity extends RefData {
  public name: string;
  public type: ActivityType;
}
