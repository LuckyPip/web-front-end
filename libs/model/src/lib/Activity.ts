export interface Activity {
  id: string
  name: string
  description: string
  startDate?: Date
  dueDate?: Date,
  pointsPossible: number,
  isGradable: boolean,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
