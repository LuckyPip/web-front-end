import {Role} from "@satipasala/model";
import {Permission} from "@satipasala/model";

export interface User {
  uid?: string,
  userName?: string | null,

  firstName?: string | null,
  lastName?: string | null,
  displayName?: string | null;
  dob?: string | null;
  nic?: string | null;
  phoneNumber?: string | null;
  email?: string | null,
  password?: string | null,

  street?: string | null;
  city?: string | null;
  province?: string | null;
  country?: string | null;
  organization?: string | null;

  userRole?: Role,
  userPermissions?: Array<Permission> | string | null,
  preferredMedium?: string | null;

  photoURL?: string | null,

  createdAt?: Date | null,
  updatedAt?: Date | null,
}
