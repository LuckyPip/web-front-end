import { Injectable } from '@angular/core';
import {CollectionService, Question, Questionnaire} from "@satipasala/base";
import {AngularFirestore} from "@angular/fire/firestore";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class QuestionsService extends CollectionService<Question<any>> {

  public static collection: string = "questions";

  constructor(protected fireStore: AngularFirestore) {
    super(QuestionsService.collection, fireStore);
  }
}

