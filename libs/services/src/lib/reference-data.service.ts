import {Injectable} from '@angular/core';

import {AngularFirestore} from "@angular/fire/firestore";
import {Label, LocationType} from "@satipasala/model";
import {BehaviorSubject, Observable} from 'rxjs';
import {ServicesModule} from "@satipasala/services";
import {RefData} from "../../../model/src/lib/referencedata/RefData";
import {QuestionLevel, QuestionType, ScoringMechanism} from "../../../base/src/lib/questions/model/Question";
import {Option} from "@satipasala/base";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class ReferenceDataService {

  locationTypes: LocationType[];
  private collection: string = "reference_data";
  private referenceDataMap: Map<RefDataType, any> = new Map();

  constructor(protected fireStore: AngularFirestore) {
  }


  /**
   * This method checks whether there is a subscription to the requested reference data type. If it is, observable will
   * return the last known values from the cache while subscribing to real time updates. If there is no subscription
   * (i.e. this is very first request for this reference data type), create a new subscription to the firestore for real
   * time updates and send the latest data once receive from server. Subsequently all the subscribers are kept updated with
   * the real time values.
   *
   * ##IMPORTANT:
   * As this returns an observable with stream of data, it is important to **unsubscribe** whenever subscription
   * is **not** required. Failure to do so will lead to a memory leak.
   *
   * ### Examples
   * ### Get data once without real time updates
   *   this.refDataService.getData("activityTypes").subscribe(activityArray => {
   *       console.log( activityArray);
   *   }).unsubscribe(); //Unsubscribe at the end if values are needed only once
   *
   * ### Multiple subscriptions by unsubscribing to the existing
   *   var activityObservable = this.refDataService.getData("activityTypes").subscribe(activityArray => {
   *       console.log( activityArray);
   *   });
   *   ...
   *   activityObservable.unsubscribe(); //Unsubscribe from existing subscription if above code gets repeated.
   *
   * ### Subscribe with real time updates
   * (All the available values will be sent whenever subscribing document is updated)
   *
   *   this.refDataService.getData(RefDataType.ACTIVITY).subscribe(activityArray => {
   *       console.log( activityArray);
   *   });
   *
   * @param {RefDataType} dataType - @see {@link RefDataType}
   * @returns {Observable<T[]>}
   */
  public getData<T extends RefData>(dataType: RefDataType): Observable<T[]> {
    if (this.referenceDataMap.get(dataType) == null) {
      this.referenceDataMap.set(dataType, new BehaviorSubject<RefData[]>([]));
      this.fireStore.firestore.collection("referencedata").doc(dataType).onSnapshot(documentSnapshot => {
        var source = documentSnapshot.metadata.fromCache ? "local cache" : "server";
        console.log("********** Source of data: " + source);
        const data = documentSnapshot.data();
        const dataArray: T[] = [];
        for (let key of Object.keys(data)) {
          dataArray.push(data[key]);
        }
        this.referenceDataMap.get(dataType).next(dataArray);
      });
    }

    return this.referenceDataMap.get(dataType);
  }


  public getHostTypes(typeconsumer) {

    return this.fireStore.collection(this.collection).doc("host").valueChanges().subscribe(action => typeconsumer(action));
  }

  public getLocationTemplateByName(templateName: string): Observable<any> {

    return this.fireStore.collection(this.collection).doc("host").collection("location_templates",
      query => query.where('host_type', '==', templateName).limit(1))
      .get()
  }


  public getQuestionLevels(): Array<QuestionLevel> {
    return [
      <QuestionLevel>{
        name: "Beginner",
        color: "primary"
      }, <QuestionLevel>{
        name: "Intermediate",
        color: "accent"
      }, <QuestionLevel>{
        name: "Advanced",
        color: "warn"
      }
    ]
  }

  public getQuestionTypes(): Array<QuestionType<any>> {
    return [
      <QuestionType<any>>{
        id: "1",
        name: "Multiple Choice",
        type: "MULTIPLE_CHOICE"
      }, <QuestionType<any>>{
        id: "2",
        name: "Written",
        type: "WRITTEN"
      }
    ]
  }

  getQuestionScoringMechanisms(): ScoringMechanism[] {
    return [
      {
        id: "",
        name: "",
        description: "",
        type: "SCALE"
      },

      {
        id: "",
        name: "",
        description: "",
        type: "REVERSE_SCALE"
      }
    ]
  }


  getQuestionLabels(): Label[] {
    return [
      {
        type: "",
        category: "",
        name: "Daham Paal"
      }, {
        type: "",
        category: "",
        name: "Mindfull Game"
      },
      {
        type: "",
        category: "",
        name: "Waking Practice"
      },
      {
        type: "",
        category: "",
        name: "Below year 7"
      },
      {
        type: "",
        category: "",
        name: "Organization Only"
      }
    ]

  }

  getOptionsByQuetionType(questionType: String):Option<any>[] {
    switch (questionType) {
      case "RARELY_SOMETIMES":
        return [
          <Option<Number>>{
            imageUrl: "",
            label: "Rarely/Not at all",
            value: 1
          },
          <Option<Number>>{
            imageUrl: "",
            label: "Sometimes",
            value: 2
          },
          <Option<Number>>{
            imageUrl: "",
            label: "Often",
            value: 3
          },
          <Option<Number>>{
            imageUrl: "",
            label: "Almost",
            value: 4
          }]
        break;
      case "NEVER_ALOT":
        return [<Option<Number>> {
          imageUrl: "",
          label: "Never",
          value: 1
        }, <Option<Number>> {
          imageUrl: "",
          label: "A Little",
          value: 2
        }, <Option<Number>> {
          imageUrl: "",
          label: "Sometimes",
          value: 3
        }, <Option<Number>> {
          imageUrl: "",
          label: "A Lot",
          value: 4
        }]
        break;
      case "ALMOST_NEVER":
        return [<Option<Number>> {
          imageUrl: "",
          label: "Almost Always",
          value: 1
        }, <Option<Number>> {
          imageUrl: "",
          label: "Very Frequently",
          value: 2
        }, <Option<Number>> {
          imageUrl: "",
          label: "Somewhat frequently",
          value: 3
        }, <Option<Number>> {
          imageUrl: "",
          label: "somewhat infrequently",
          value: 4
        }, <Option<Number>> {
          imageUrl: "",
          label: "Very Infrequently",
          value: 5
        }, <Option<Number>> {
          imageUrl: "",
          label: "Almost Never",
          value: 5
        }]

    }
  }


}

/**
 * Reference data types
 *
 */
export enum RefDataType {
  ACTIVITY_TYPE = "activityTypes",
  ACTIVITY = "activities",
  PROGRAM = "programs"
}
