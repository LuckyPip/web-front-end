import {NgModule} from '@angular/core';
import {AngularFirestore, AngularFirestoreModule} from "@angular/fire/firestore";
import {ReferenceDataService} from "./reference-data.service";
import {AuthService} from "./auth.service";
import {HostsService} from "./hosts.service";
import {NotifyService} from "./notify.service";
import {AngularFireAuth, AngularFireAuthModule} from "@angular/fire/auth";
import {CoursesService} from "./courses.service";
import {ActivitiesService} from "./activities.service";
import {QuestionnaireService} from "./questionnaire.service";
import { LocationsService } from './locations.service';
import {QuestionsService} from "@satipasala/services";

@NgModule({
  providers: [
    AngularFireAuth,
    AngularFirestore,
    ReferenceDataService,
    HostsService,
    LocationsService,
    AuthService,
    NotifyService,CoursesService,
    ActivitiesService,
    QuestionnaireService,
    QuestionsService,
    ReferenceDataService
  ],
  imports: [
    AngularFireAuthModule,
    AngularFirestoreModule
  ]
})
export class ServicesModule {
}
