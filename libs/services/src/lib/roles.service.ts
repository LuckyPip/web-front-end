import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference, QueryFn} from "@angular/fire/firestore";
import {Role} from "../../../model/src/lib/Role";
import {CollectionService} from "@satipasala/base";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class RolesService extends CollectionService<Role> {
  public static collection: string = "roles";

  constructor(protected fireStore: AngularFirestore) {
    super(RolesService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getRole(roleId, roleConsumer) {
    return this.fireStore.collection(this.collection).doc(roleId).valueChanges().subscribe(action => roleConsumer(action));
  }
}
