import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference, QueryFn} from "@angular/fire/firestore";
import {HostLocation} from "../../../model/src/lib/HostLocation";
import {CollectionService} from "@satipasala/base";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class LocationsService extends CollectionService<HostLocation> {
  public static collection: string = "locations";

  constructor(protected fireStore: AngularFirestore) {
    super(LocationsService.collection, fireStore);
  }
}
