import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {auth} from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import {NotifyService} from './notify.service';

import {Observable, of} from 'rxjs';
import {switchMap, startWith, tap, filter} from 'rxjs/operators';

/*******************************************
 * Temporary Models
 ********************************************/
import {User, Role} from '@satipasala/model';
import * as firebase from "firebase";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class AuthService {
  user: Observable<User | null>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private notify: NotifyService
  ) {

    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
      // tap(user => localStorage.setItem('user', JSON.stringify(user))),
      // startWith(JSON.parse(localStorage.getItem('user')))
    );
  }

  /*******************************************
   * Email-Password Auth
   ********************************************/

  registerUserByAdmin(
    firstName: string,
    lastName: string,
    displayName: string,
    userRole: Role,
    email: string,
    password: string,
  ) {
    const userData: User = {
      firstName: firstName,
      lastName: lastName,
      displayName: displayName,
      userRole: userRole,
      email: email,
      password: password,
    };
    return this.afs.collection("users").doc(email).set(userData);
  }

  createUserByAdmin(userData: User) {
    return this.afs.collection("users").doc(userData.email).set(userData);
  }

  createFirebaseUser(email: string, password: string) {

    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
  }

  emailSignUp(email: string, password: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(credential => {
        this.notify.update(
          'Welcome new user!',
          'success'
        );
        return this.updateUserData(credential.user); // if using firestore
      })
      .catch(error => this.handleError(error));
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
        this.notify.update('Welcome back!', 'success');
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = auth();

    return fbAuth
      .sendPasswordResetEmail(email)
      .then(() => this.notify.update(
        'Password update email sent',
        'info'
      ))
      .catch(error => this.handleError(error));
  }

  /*******************************************
   * OAuth Methods
   ********************************************/

  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  facebookLogin() {
    const provider = new auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }

  twitterLogin() {
    const provider = new auth.TwitterAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        this.notify.update(
          'Welcome to Sati Pasala!',
          'success'
        );
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  /*******************************************
   * Common Helper Functions
   ********************************************/

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
    this.notify.update(error.message, 'error');
  }

  // Sets user data to firestore after successful login
  private updateUserData(user: User) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${user.uid}`
    );
    const data: User = {
      uid: user.uid,
      email: user.email || null,
      displayName: user.displayName || 'New User',
      photoURL: user.photoURL || 'https://goo.gl/Fz9nrQ'
    };
    return userRef.set(data);
  }
}
