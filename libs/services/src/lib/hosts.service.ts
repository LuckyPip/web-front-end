import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference, QueryFn} from "@angular/fire/firestore";
import {Host} from "../../../model/src/lib/Host";
import {CollectionService} from "@satipasala/base";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class HostsService extends CollectionService<Host> {
  public static collection: string = "hosts";

  constructor(protected fireStore: AngularFirestore) {
    super(HostsService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getHost(hostId, hostConsumer) {
    return this.fireStore.collection(this.collection).doc(hostId).valueChanges().subscribe(action => hostConsumer(action));
  }
}
