import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference, QueryFn} from "@angular/fire/firestore";
import {Permission} from "../../../model/src/lib/Permission";
import {CollectionService} from "@satipasala/base";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class PermissionsService extends CollectionService<Permission> {
  public static collection: string = "permissions";

  constructor(protected fireStore: AngularFirestore) {
    super(PermissionsService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getPermission(permissionId, permissionConsumer) {
    return this.fireStore.collection(this.collection).doc(permissionId).valueChanges().subscribe(action => permissionConsumer(action));
  }
}
