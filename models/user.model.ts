import { Base } from './base.model';

export class User extends Base {
  email?: string | null;
  username?: string | null; // this field is important for users without firebase auth credentials
  photoURL?: string;
  displayName?: string;
  userRole?: string[];
  userPermissions?: string[];

  constructor(object: object = {}) {
    super(object);
  }
}
