export const DASHBOARD_ROUTE = "dashboard";
export const DASHBOARD_OVERVIEW_ROUTE = "overview";

export const USERS_ROUTE = "users";
export const USERS_LOGIN_ROUTE = "login";
export const USERS_MANAGEMENT_ROUTE = "management";
export const USERS_EDIT_ROUTE = ":uid";
export const USERS_REGISTER_ROUTE = "register";

export const HOST_MANAGEMENT_ROUTE = "host";
export const HOST_MANAGEMENT_INFO_ROUTE = "";
export const HOST_MANAGEMENT_EDIT_ROUTE = ":hostId";
export const HOST_LOCATIONS_INFO_ROUTE = ":hostId/location";
export const HOST_LOCATIONS_EDIT_ROUTE = HOST_LOCATIONS_INFO_ROUTE + "/:locationId";


export const COURSE_MANAGEMENT_ROUTE = "cources";
export const COURSE_MANAGEMENT_ACTION_ROUTE = ":courseId/:action";  // edit, view
export const COURSE_MANAGEMENT_ADD_ROUTE = ":courseId";             // new course
// Following 2 are unused at the moment
export const COURSE_ACTIVITY_INFO_ROUTE: string = COURSE_MANAGEMENT_ADD_ROUTE + "/activities";
export const COURSE_ACTIVITY_EDIT_ROUTE: string = COURSE_ACTIVITY_INFO_ROUTE + "/:activityId";

export const ACTIVITY_MANAGEMENT_ROUTE = "activities";
export const ACTIVITY_MANAGEMENT_ACTION_ROUTE = ":activityId/:action";  // edit, view
export const ACTIVITY_MANAGEMENT_EDIT_ROUTE = ":activityId";            // new course
export const ACTIVITY_COURSE_INFO_ROUTE: string = ACTIVITY_MANAGEMENT_EDIT_ROUTE + "/courses";
export const ACTIVITY_COURSE_EDIT_ROUTE: string = ACTIVITY_COURSE_INFO_ROUTE + "/:courseId";


export const FILE_MANAGEMENT_ROUTE = "files";
export const FILE_MANAGEMENT_IMG_UPLOAD_ROUTE = "imageUploads";
export const FILE_MANAGEMENT_PDF_UPLOAD_ROUTE = "pdfUploads";
export const FILE_MANAGEMENT_AUDIO_FILES_ROUTE = "audioFiles";


export const QUESTIONNAIRE_MANAGEMENT_ROUTE = "questionnaires";

export const QUESTIONNAIRE_MANAGEMENT_EDIT_ROUTE = "edit/:questionnaireId";
export const QUESTIONNAIRE_MANAGEMENT_INFO_ROUTE = "info/:questionnaireId";

export const QUESTION_MANAGEMENT_ROUTE = "questions";
export const QUESTION_MANAGEMENT_ACTION_ROUTE = ":questionId/:action";
export const QUESTION_MANAGEMENT_ADD_ROUTE = "add";


export const TRANSLATION_ROUTE = "translations";
export const GLOSSARY_ROUTE = "glossary";

export const AUTH_MANAGEMENT_ROUTE = "auth";
export const AUTH_MANAGEMENT_ROUTE_PERMISSIONS = "permissions";
export const AUTH_MANAGEMENT_ROUTE_ROLES = "roles";
export const AUTH_MANAGEMENT_ROUTE_ROLES_FORM = "add/edit";
export const AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_RELATIVE = "add";
export const AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_ABS = "roles/add";
export const AUTH_MANAGEMENT_ROUTE_ROLES_PERMISSION = AUTH_MANAGEMENT_ROUTE_ROLES + "/:roleId/permissions";
