import { Component, OnInit, Input } from '@angular/core';
import {Router} from "@angular/router";
import { COURSE_ACTIVITY_INFO_ROUTE, COURSE_ACTIVITY_EDIT_ROUTE, COURSE_MANAGEMENT_ROUTE } from 'apps/admin/src/app/app-routs';

import {ActivitiesService} from "@satipasala/services";
import {Activity} from "@satipasala/model";

@Component({
  selector: 'admin-activity-sub-menu',
  templateUrl: './course-activity-sub-menu.component.html',
  styleUrls: ['./course-activity-sub-menu.component.css']
})
export class CourseActivitySubMenuComponent implements OnInit {

  constructor(private router: Router) { }

  @Input() activity : Activity;

  ngOnInit() {
  }

  editActivity(){
    this.router.navigate([COURSE_MANAGEMENT_ROUTE + '/' + COURSE_ACTIVITY_EDIT_ROUTE]);
  }

  viewActivity(){
    this.router.navigate([COURSE_MANAGEMENT_ROUTE + '/' + COURSE_ACTIVITY_INFO_ROUTE]);
  }

  viewAllCourses() {

  }
}
