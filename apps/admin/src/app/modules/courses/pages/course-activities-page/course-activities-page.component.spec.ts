import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CourseActivitiesPage } from './course-activities-page.component';
import { MaterialModule } from 'apps/admin/src/app/imports/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import {ActivitiesService} from "@satipasala/services";
import {ActivatedRoute, Params} from "@angular/router";
import {of} from "rxjs";
import {AngularFirestore} from "@angular/fire/firestore";
import {MockFireStore} from "@satipasala/testing";
import {CourseActivityListComponent} from "../../components/course-activity-list/course-activity-list.component";

describe('CourseActivitiesPage', () => {
  let component: CourseActivitiesPage;
  let fixture: ComponentFixture<CourseActivitiesPage>;
  let params:Params = {id: 1}
  const fakeActivatedRoute = {
    params: of(params)
  } as ActivatedRoute;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[ActivitiesService,{provide: ActivatedRoute, useValue: fakeActivatedRoute},{provide: AngularFirestore, useValue: MockFireStore}],
      declarations: [ CourseActivitiesPage ,CourseActivityListComponent],
      imports: [
        MaterialModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseActivitiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
