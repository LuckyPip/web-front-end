import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule ,
  MatMenuModule,
  MatIconModule,
  MatSort
} from '@angular/material';
import { EditCoursePage } from './edit-course-page.component';
import { CourseActivityListComponent } from '../../components/course-activity-list/course-activity-list.component';
import {CourseActivitySubMenuComponent} from '../../components/course-activity-sub-menu/course-activity-sub-menu.component';

xdescribe('CoursePageComponent', () => {
  let component: EditCoursePage;
  let fixture: ComponentFixture<EditCoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCoursePage, CourseActivityListComponent, CourseActivitySubMenuComponent ],
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatMenuModule,
        MatIconModule
      ],
      providers:[MatSort]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
