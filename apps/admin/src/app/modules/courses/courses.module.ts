import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesPage } from './pages/cources-page/courses-page.component';
import { CourceStatsComponent } from './pages/cource-stats/cource-stats.component';

import {
  MatInputModule,
  MatButtonModule,
  MatSelectModule,
  MatRadioModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatMenuModule,
  MatIconModule,
} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import { CourseSubMenuComponent } from './components/course-sub-menu/course-sub-menu.component';
import { EditCoursePage } from './pages/course-edit-page/edit-course-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CourseActivityListComponent } from './components/course-activity-list/course-activity-list.component';
import { CourseActivitySubMenuComponent } from './components/course-activity-sub-menu/course-activity-sub-menu.component';
import { CoursesService, ServicesModule} from "@satipasala/services";
import {DynamicFormsModule, ErrorStateMatcherFactory} from "@satipasala/base";
import {CourseActivitiesPage} from "./pages/course-activities-page/course-activities-page.component";
import {CourseFormComponent} from "./components/course-form/course-form.component";

@NgModule({
  declarations: [CoursesPage, CourceStatsComponent, CourseSubMenuComponent, EditCoursePage,CourseActivityListComponent,CourseActivitySubMenuComponent,
    CourseFormComponent,CourseActivitiesPage],
  imports: [
    CommonModule,
    DynamicFormsModule,
    ServicesModule,
    CoursesRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[CoursesService,ErrorStateMatcherFactory]
})
export class CoursesModule { }
