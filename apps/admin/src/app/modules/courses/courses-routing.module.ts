import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesPage } from './pages/cources-page/courses-page.component';
import { EditCoursePage } from './pages/course-edit-page/edit-course-page.component';
import { COURSE_MANAGEMENT_ACTION_ROUTE, COURSE_ACTIVITY_INFO_ROUTE, COURSE_MANAGEMENT_ADD_ROUTE } from '../../app-routs';
import {CourseActivitiesPage} from "./pages/course-activities-page/course-activities-page.component";

const routes: Routes = [
  {path: '', component: CoursesPage},
  {path: COURSE_MANAGEMENT_ACTION_ROUTE, component: EditCoursePage},
  {path: COURSE_MANAGEMENT_ADD_ROUTE, component: EditCoursePage},
  {path: COURSE_ACTIVITY_INFO_ROUTE, component: CourseActivitiesPage}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
