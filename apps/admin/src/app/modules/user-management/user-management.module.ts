import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserManagementRoutingModule} from './user-management-routing.module';
import {UserEditFormComponent} from './components/user-edit-form/user-edit-form.component';
import {
  MatInputModule,
  MatButtonModule,
  MatSelectModule,
  MatRadioModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatMenuModule,
  MatIconModule,
  MatFormFieldModule,
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {UserManagementPageComponent} from './pages/user-management-page/user-management-page.component';
import {UserSubMenuComponent} from './components/user-sub-menu/user-sub-menu.component';
import {UserRegistrationPageComponent} from "./pages/user-registration-page/user-registration-page.component";
import {CoreModule} from "../core/core.module";
import {AuthService, UsersService, ServicesModule} from "@satipasala/services";
import {AuthGuard, GuardsModule} from "@satipasala/guards";
import {UserTableComponent} from './components/user-table/user-table.component';
import {UserEditPageComponent} from './pages/user-edit-page/user-edit-page.component';
import {CoreComponentsModule} from "@satipasala/base";
import { TextSearchComponent } from './components/text-search/text-search.component';

@NgModule({
  declarations: [UserManagementPageComponent, UserSubMenuComponent, UserRegistrationPageComponent, UserTableComponent, UserEditFormComponent, UserEditPageComponent, TextSearchComponent],
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    CoreModule,
    ServicesModule,
    GuardsModule,
    CoreComponentsModule,
  ],
  providers: [AuthService, AuthGuard, UsersService]
})
export class UserManagementModule {
}
