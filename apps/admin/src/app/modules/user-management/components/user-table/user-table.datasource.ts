import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {User} from "@satipasala/model";
import {AfterViewInit} from "@angular/core";
import {FirebaseDataSource} from "@satipasala/base";
import {UsersService} from "@satipasala/services";

/**
 * Data source for the UserManagementPage view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class UserTableDatasource extends FirebaseDataSource<User> implements AfterViewInit {
  // userId: string;
  // public static subCollection:string = "roles";

  constructor(private paginator: MatPaginator, private sort: MatSort,
              public usersService: UsersService) {
    super(paginator, sort, usersService);
  }

  // loadMore(event: PageEvent) {
  //   this.queryData(event.pageIndex, event.pageSize, {documentId: this.userId, subCollection:UserManagementPageDataSource.subCollection});
  // }
  //
  // ngAfterViewInit(): void {
  //   this.queryData(0, this.paginator.pageSize, {documentId: this.userId, subCollection:UserManagementPageDataSource.subCollection});
  // }

  loadMore(event: PageEvent) {
    this.queryData(query => query.orderBy("uid").startAt(event.pageIndex).limit(event.pageSize));
  }

  ngAfterViewInit(): void {
    this.queryData(query => query.orderBy("uid").startAt(0).limit(this.matPaginator.pageSize));
  }

}
