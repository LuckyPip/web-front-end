import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {UsersService} from "@satipasala/services";
import {FirebaseDataSource} from "@satipasala/base";
import {Host, Role, User} from "@satipasala/model";
import {UserTableDatasource} from "./user-table.datasource";

@Component({
  selector: 'admin-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource: FirebaseDataSource<User>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['displayName', 'status', 'userRole', 'organizationType' ,'edit'];
  roles: Role[] = [
    {
      id: "SYSTEMADMIN",
      name: "System Administrator",
    },
    {
      id: "READONLY",
      name: "Read only access to the system",
    },
  ];

  status = ['Active', 'Inactive'];

  orgType = ['School', 'Temple'];

  pageEvent: PageEvent;

  constructor(private usersService: UsersService) {
  }

  ngOnInit() {
    this.dataSource = new UserTableDatasource(this.paginator, this.sort, this.usersService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  applyFilter(filterValue: string) {
    this.dataSource["filter"] = filterValue.trim().toLowerCase();
  }
}
