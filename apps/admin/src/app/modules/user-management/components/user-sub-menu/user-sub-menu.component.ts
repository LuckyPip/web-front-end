import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {USERS_EDIT_ROUTE, USERS_MANAGEMENT_ROUTE, USERS_ROUTE} from "../../../../app-routs";
import {User} from "@satipasala/model";

@Component({
  selector: 'admin-user-sub-menu',
  templateUrl: './user-sub-menu.component.html',
  styleUrls: ['./user-sub-menu.component.scss']
})
export class UserSubMenuComponent implements OnInit {

  @Input() user: User;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  edit() {
    this.router.navigateByUrl(USERS_ROUTE + "/" + this.user.uid);
  }

  showMore() {

  }
}
