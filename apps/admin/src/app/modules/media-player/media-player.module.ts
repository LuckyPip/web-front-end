import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import {MatCardModule} from "@angular/material";

import { MediaPlayerRoutingModule } from './media-player-routing.module';
import { AudioPlayerComponent } from './components/audio-player/audio-player.component';
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';
import {UploadsModule} from "../uploads/uploads.module";

@NgModule({
  declarations: [AudioPlayerComponent, VideoPlayerComponent, ImageViewerComponent],
  imports: [
    CommonModule,
    MediaPlayerRoutingModule,
    NgxAudioPlayerModule,
    MatCardModule,
    UploadsModule
  ]
})
export class MediaPlayerModule { }
