import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OverviewPageComponent} from "./pages/overview/overview-page.component";
import {DASHBOARD_OVERVIEW_ROUTE} from "../../app-routs";
import {AuthGuard} from "@satipasala/guards";

const routes: Routes = [

  {path: '', redirectTo:DASHBOARD_OVERVIEW_ROUTE,pathMatch: 'full'},
  {path: DASHBOARD_OVERVIEW_ROUTE, component: OverviewPageComponent,canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
