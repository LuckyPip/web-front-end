import {Component, OnInit} from '@angular/core';
import {
  ChipList,
  DragDropList,
  DragDropListItem,
  Questionnaire,
} from "@satipasala/base";
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionnaireService} from "@satipasala/services";
import {BaseQuestionnairePage} from "../base-questionnaire-page";

@Component({
  selector: 'admin-questionnaire-edit-page',
  templateUrl: './questionnaire-edit-page.component.html',
  styleUrls: ['./questionnaire-edit-page.component.scss']
})
export class QuestionnaireEditPage extends BaseQuestionnairePage {


  done: Questionnaire = <Questionnaire>{
    name: "Questions?",
    questions: [
      { id:"1",
        name: 'Get up', formChipList: <ChipList>{
          stacked: true,
          type: "category",
          values: [
            {name: 'Beginner', color: 'primary'},
            {name: 'Activity', color: 'warn'}]
        }
      }
      , {
        id:"2",
        name: 'Brush teeth'
      }
      ]
  };


  constructor(protected router: Router, protected route: ActivatedRoute,
              protected questionnaireService: QuestionnaireService) {
    super(router, route, questionnaireService);
  }

  onQuestionnaireFetch(questionnaire: Questionnaire) {

  }


}
