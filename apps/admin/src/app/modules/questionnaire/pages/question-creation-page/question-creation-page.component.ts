import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {
  Question,
  QuestionTypeAlmostNever,
  QuestionTypeNeverAlot,
  QuestionTypeRarelySometimes,
  QuestionTypeWriten
} from "@satipasala/base";
import {QuestionnaireService, QuestionsService, ReferenceDataService} from "@satipasala/services";
import {Label} from "@satipasala/model";
import {
  QuestionLevel,
  QuestionType,
  ScoringMechanism
} from "../../../../../../../../libs/base/src/lib/questions/model/Question";

import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'admin-question-creation-page',
  templateUrl: './question-creation-page.component.html',
  styleUrls: ['./question-creation-page.component.scss']
})
export class QuestionCreationPageComponent implements OnInit {
  question: Question<any> = <Question<any>> {
    labels: []
  };
  questionsTypes: QuestionType<any>[] = [new QuestionTypeAlmostNever(), new QuestionTypeNeverAlot(), new QuestionTypeRarelySometimes(), new QuestionTypeWriten()];
  questionName: string;
  questionTypes: QuestionType<any>[];
  questionLevels: QuestionLevel[];
  scoringMechanisms: ScoringMechanism[];
  questionLabels: Label[];

  questionId?: string;


  questionForm = this.fb.group({
    questionType: [null, Validators.required],
    description: [null, Validators.required],
    questionLevel: [null, Validators.required],
    scoringMechanism: [null, Validators.required]
    /* firstName: [null, Validators.required],
     lastName: [null, Validators.required],
     address: [null, Validators.required],
     address2: null,
     city: [null, Validators.required],
     postalCode: [null, Validators.compose([
       Validators.required, Validators.minLength(5), Validators.maxLength(5)])
     ],*/
  });


  constructor(private fb: FormBuilder, private referenceDataServie: ReferenceDataService,
              private questionsService: QuestionsService, private _snackBar: MatSnackBar, private route: ActivatedRoute) {
    this.questionLabels = this.referenceDataServie.getQuestionLabels();
    this.questionTypes = this.referenceDataServie.getQuestionTypes();
    this.scoringMechanisms = this.referenceDataServie.getQuestionScoringMechanisms();
    this.questionLevels = this.referenceDataServie.getQuestionLevels();
    /*this.question.labels = [
      {
        category: "question_group",
        type: "chip",
        value: "Beginner"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Advanced"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Intermediate"
      }
    ];*/


  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.questionId) {
        this.questionId = params.questionId;
        if (this.questionId != null) {
          this.questionsService.get(this.questionId).subscribe(question => {
            this.fillForm(question);
          });

          if (params.action === 'info') {
            this.questionForm.disable();
          } else if (params.action === 'edit') {
            this.questionForm.enable();
          }
        } else {
          this.questionForm.enable();
        }
      } else {
        this.questionForm.enable();
      }
    });
  }

  onLabelChange(checked, label: Label) {
    if (checked == true) {
      this.addLabel(label);
    } else {
      this.removeLabel(label);
    }
  }


  addLabel(label: Label) {
    this.question.labels.push(label);
  }

  removeLabel(label: Label) {
    let index = this.question.labels.indexOf(label);
    this.question.labels.splice(index);
  }

  questionNotification(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  fillForm(question: Question<any>) {
   /* for (let field in question) {
      this.questionForm.controls[field].setValue(question[field], {emitEvent: true});
    }
*/
    this.question = Object.assign(this.question, question);
  }

  onSubmit(values) {
    this.question = Object.assign(this.question, values);
    this.questionsService.add(this.question).then(value => {
      if (value) {
        this.questionNotification("Question Creation", "Success");
      } else {
        this.questionNotification("Question Creation", "Failed");
      }
    });
  }

  onSelectedQuestionChange(questionType: QuestionType<any>) {
    this.question.options = this.referenceDataServie.getOptionsByQuetionType(questionType.answerType);
  }
}
