import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  QUESTIONNAIRE_MANAGEMENT_EDIT_ROUTE,
  QUESTIONNAIRE_MANAGEMENT_INFO_ROUTE,
  QUESTIONNAIRE_MANAGEMENT_ROUTE,
  QUESTION_MANAGEMENT_ROUTE, QUESTION_MANAGEMENT_ACTION_ROUTE, QUESTION_MANAGEMENT_ADD_ROUTE
} from "../../app-routs";
import {QuestionnaireListPage} from "./pages/questionnaire-list-page/questionnaire-list-page.component";
import {QuestionsPage} from "./pages/questions-page/questions-page.component";
import {QuestionnaireEditPage} from "./pages/questionnaire-edit-page/questionnaire-edit-page.component";
import {QuestionnaireInfoPage} from "./pages/questionnaire-info-page/questionnaire-info-page.component";
import {QuestionCreationPageComponent} from "./pages/question-creation-page/question-creation-page.component";


const routes: Routes = [
  {path: '', redirectTo:QUESTIONNAIRE_MANAGEMENT_ROUTE, pathMatch:'full'},
  {path: QUESTIONNAIRE_MANAGEMENT_ROUTE, component: QuestionnaireListPage},
  {path: QUESTIONNAIRE_MANAGEMENT_EDIT_ROUTE, component: QuestionnaireEditPage},
  {path: QUESTIONNAIRE_MANAGEMENT_INFO_ROUTE, component: QuestionnaireInfoPage},
  {path: QUESTION_MANAGEMENT_ROUTE, component: QuestionsPage},
  {path: QUESTION_MANAGEMENT_ROUTE+"/"+QUESTION_MANAGEMENT_ACTION_ROUTE, component: QuestionCreationPageComponent},
  {path: QUESTION_MANAGEMENT_ROUTE+"/"+QUESTION_MANAGEMENT_ADD_ROUTE, component: QuestionCreationPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionnaireRoutingModule { }
