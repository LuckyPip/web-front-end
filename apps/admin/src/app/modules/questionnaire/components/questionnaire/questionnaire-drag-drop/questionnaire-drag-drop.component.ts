import {Component, Input, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Questionnaire} from "@satipasala/base";
import {QuestionnaireService} from "@satipasala/services";

@Component({
  selector: 'admin-questionnaire-drag-drop',
  templateUrl: './questionnaire-drag-drop.component.html',
  styleUrls: ['./questionnaire-drag-drop.component.scss']
})
export class QuestionnaireDragDropComponent implements OnInit {
  @Input() questionnaire:Questionnaire;
  constructor(private questionnaireService:QuestionnaireService) { }

  ngOnInit() {
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.questionnaireService.update(this.questionnaire.id,this.questionnaire).then(value => {
      console.log(value);
    })
  }


}
