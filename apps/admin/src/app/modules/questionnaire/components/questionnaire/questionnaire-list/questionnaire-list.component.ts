import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionnaireService} from "@satipasala/services";
import {QuestionnaireDataSource} from "./questionnaire-data-source";

@Component({
  selector: 'admin-questionnaire-list',
  templateUrl: './questionnaire-list.component.html',
  styleUrls: ['./questionnaire-list.component.scss']
})
export class QuestionnaireListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: QuestionnaireDataSource;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id','name', 'edit'];
  constructor(private  questionnaireService:QuestionnaireService){

  }

  ngOnInit() {
    this.dataSource = new QuestionnaireDataSource(this.paginator, this.sort, this.questionnaireService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }
}
