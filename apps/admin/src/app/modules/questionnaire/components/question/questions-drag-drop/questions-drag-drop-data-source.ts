import {FirebaseDataSource, Question} from "@satipasala/base";
import {MatPaginator, MatSort} from "@angular/material";
import {QuestionsService} from "@satipasala/services";

export class QuestionsDragDropDataSource extends FirebaseDataSource<Question<any>>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public questionnaireService: QuestionsService) {
    super(paginator, sort, questionnaireService);
  }

  fetchData(): void{
    this.queryData(query => query.orderBy("label").startAt(0).limit(this.matPaginator.pageSize));
  }

}
