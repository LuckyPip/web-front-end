import {Component, Input, OnInit} from '@angular/core';
import {QUESTION_MANAGEMENT_ROUTE} from "../../../../../app-routs";
import {ActivatedRoute, Router} from "@angular/router";
import {Question} from "@satipasala/base";

@Component({
  selector: 'admin-question-sub-menu',
  templateUrl: './question-sub-menu.component.html',
  styleUrls: ['./question-sub-menu.component.scss']
})
export class QuestionSubMenuComponent implements OnInit {
  @Input() question: Question<any>;
  constructor(private router: Router, private activiatedRoute:ActivatedRoute) { }

  ngOnInit() {
  }


  editQuestion() {
    //this.router.navigate([QUESTIONNAIRE_MANAGEMENT_ROUTE + "/edit/" + this.questionnaire.id]);
    this.router.navigate([this.question.id+"/edit"],{relativeTo:this.activiatedRoute});
  }

  viewQuestion() {
    this.router.navigate([  this.question.id+"/info"],{relativeTo:this.activiatedRoute});
    /*,relativeTo: this.activatedRoute*/
  }

}
