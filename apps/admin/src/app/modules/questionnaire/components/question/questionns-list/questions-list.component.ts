import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionsService} from "@satipasala/services";
import {QuestionsListDataSource} from "./questions-list-data-source";
import {ActivatedRoute, Router} from "@angular/router";
import {QUESTION_MANAGEMENT_ACTION_ROUTE, QUESTION_MANAGEMENT_ADD_ROUTE} from "../../../../../app-routs";
import {Question} from "@satipasala/base";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'admin-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class QuestionsListComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource: QuestionsListDataSource;
  expandedQuestion: Question<any> | null;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['description','questionLevel','questionType','scoringMechanism', 'label', 'edit'];

  constructor(private  questionsService: QuestionsService, private router: Router, private activiatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.dataSource = new QuestionsListDataSource(this.paginator, this.sort, this.questionsService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  addNewQuestion() {
    this.router.navigate([QUESTION_MANAGEMENT_ADD_ROUTE], {relativeTo: this.activiatedRoute});
  }

  onRowClick(row) {
    this.expandedQuestion = this.expandedQuestion === row ? null : row;
  }
}
