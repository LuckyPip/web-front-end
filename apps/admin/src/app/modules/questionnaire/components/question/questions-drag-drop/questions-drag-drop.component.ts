import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Chip, ChipList, Question, Questionnaire} from "@satipasala/base";
import {QuestionsService, ReferenceDataService} from "@satipasala/services";
import {QuestionsDragDropDataSource} from "./questions-drag-drop-data-source";
import {MatPaginator, PageEvent} from "@angular/material";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {QuestionLevel} from "../../../../../../../../../libs/base/src/lib/questions/model/Question";
import {QUESTION_MANAGEMENT_ADD_ROUTE} from "../../../../../app-routs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'admin-questions-drag-drop',
  templateUrl: './questions-drag-drop.component.html',
  styleUrls: ['./questions-drag-drop.component.scss']
})
export class QuestionsDragDropComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() questionnaire: Questionnaire;

  questionLevelsChipList: ChipList = new class implements ChipList {
    stacked: false;
    type: "Question Types";
    values: QuestionLevel[];
  }

  questions: Question<any>[];

  dataSource: QuestionsDragDropDataSource;
  //todo change this numbers to get from collection.
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private referenceDataService: ReferenceDataService,
              private questionnaireService: QuestionsService,
              private router:Router,
              private activatedRoute:ActivatedRoute) {

  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  ngOnInit() {
    this.questionLevelsChipList.values = this.referenceDataService.getQuestionLevels();
    this.dataSource = new QuestionsDragDropDataSource(this.paginator, null, this.questionnaireService);
    this.dataSource.connect().subscribe(value => {
      this.questions = value;
    })
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect()
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  onQuestionTypeClick(chip: Chip) {
    console.log(chip);
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  addNewQuestion() {
    this.router.navigate([QUESTION_MANAGEMENT_ADD_ROUTE], {relativeTo: this.activatedRoute});
  }
}
