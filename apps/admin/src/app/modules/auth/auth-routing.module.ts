import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AUTH_MANAGEMENT_ROUTE_PERMISSIONS,
  AUTH_MANAGEMENT_ROUTE_ROLES,
  AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_ABS,
  AUTH_MANAGEMENT_ROUTE_ROLES_PERMISSION

} from "../../app-routs";
import {RoleManagementPage} from "./pages/role-management/role-management-page.component";
import {PermissionManagementPage} from "./pages/permission-management/permission-management-page.component";
import {RolePermissionManagementPage} from "./pages/role-permission-management/role-permission-management-page.component";
import { AddRoleManagementComponent } from './pages/add-role-management/add-role-management.component';

const routes: Routes = [
  {path: AUTH_MANAGEMENT_ROUTE_PERMISSIONS, component: PermissionManagementPage},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES, component: RoleManagementPage},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES_PERMISSION, component: RolePermissionManagementPage},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_ABS, component: AddRoleManagementComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
