import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRoutingModule} from './auth-routing.module';
import {ServicesModule} from "@satipasala/services";

import {AuthService} from "@satipasala/services";
import {PermissionInfoComponent} from './components/permission-info/permission-info.component';
import {RoleInfoComponent} from './components/role-info/role-info.component';
import {MaterialModule} from "../../imports/material.module";
import {PermissionsService} from "@satipasala/services";
import {RolesService} from "@satipasala/services";
import {RoleSubMenuComponent} from './components/role-sub-menu/role-sub-menu.component';
import {
  MatButtonModule,
  MatCardModule, MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule
} from "@angular/material";

import {FlexLayoutModule} from "@angular/flex-layout";
import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";
import {RoleFormComponent} from './components/role-form/role-form.component';
import {RoleManagementPage} from './pages/role-management/role-management-page.component';
import {PermissionManagementPage} from './pages/permission-management/permission-management-page.component';
import {RolePermissionInfoComponent} from './components/role-permission-info/role-permission-info.component';
import {RolePermissionManagementPage} from './pages/role-permission-management/role-permission-management-page.component';
import {AddRoleFormComponent} from "./components/add-role-form/add-role-form.component";
import {AddRoleManagementComponent} from "./pages/add-role-management/add-role-management.component";

@NgModule({
  declarations: [PermissionInfoComponent, RoleInfoComponent, RoleSubMenuComponent, RoleFormComponent, RoleManagementPage, PermissionManagementPage, RolePermissionInfoComponent, RolePermissionManagementPage, AddRoleFormComponent, AddRoleManagementComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ServicesModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule, FormsModule
  ],
  providers: [AuthService, PermissionsService, RolesService]
})
export class AuthModule {
}
