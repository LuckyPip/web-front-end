import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {PermissionsService} from "@satipasala/services";
import {FirebaseDataSource} from "@satipasala/base";
import {Permission} from "@satipasala/model";

@Component({
  selector: 'admin-permission-info',
  templateUrl: './permission-info.component.html',
  styleUrls: ['./permission-info.component.scss']
})
export class PermissionInfoComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: FirebaseDataSource<Permission>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['description', 'name'];

  constructor(private permissionsService: PermissionsService) { }

  ngOnInit() {
    this.dataSource = new FirebaseDataSource<Permission>(this.paginator, this.sort, this.permissionsService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }
}
