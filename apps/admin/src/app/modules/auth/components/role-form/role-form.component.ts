import { Component, OnInit } from '@angular/core';

import {HostsService, RolesService} from "@satipasala/services";
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import {Role, Permission} from '@satipasala/model';
import * as APP_ROUTES from "../../../../app-routs";

@Component({
  selector: 'admin-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent implements OnInit {

  roleForm: FormGroup;

  public role : Role;
  public rolePermission : Permission;

  public roleId: string;

  public formTitle: string;

  public submitBtnText: string;
  constructor(private router: Router, private route : ActivatedRoute,private formBuilder : FormBuilder, private RoleService : RolesService) { }

  ngOnInit() {
    this.roleForm = this.formBuilder.group({
      id : '',
      name : '',
      image : '',
      description : ''
    });

    this.route.params.subscribe(params => {
      this.roleId = params.roleId;
      if (params.roleId === 'new') {
        this.formTitle = "Create Host";
        this.submitBtnText = "Add";
        this.role = <Role>{};
        this.setRole(this.roleForm)(this.role);
      } else {
        this.formTitle = "Edit Host";
        this.submitBtnText = "Update";
        this.RoleService.getRole(params.roleId, this.setRole(this.roleForm));
      }
    });
  }

  setRole(form) {
    return role => {
      if(this.role){
        form.setValue({
          id : role.id,
          name : role.name,
          isActive : role.isActive,
          allowedPermissions : role.allowedPermissions
        });
      } else {
        this.backToRoleManage();
      }
    };
  }

  addEditRole () {
    this.role = this.roleForm.value;
    if(this.roleId == "new") {
      this.RoleService.add(this.role);
    } else {
      this.RoleService.update(this.roleId, this.role);
    }
  }

  backToRoleManage() {
    console.log(APP_ROUTES.AUTH_MANAGEMENT_ROUTE_ROLES);
    this.router.navigateByUrl(APP_ROUTES.AUTH_MANAGEMENT_ROUTE_ROLES);
  }

}
