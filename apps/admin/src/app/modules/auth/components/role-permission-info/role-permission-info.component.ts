import {Component, OnInit} from '@angular/core';
import {Permission, Role} from "@satipasala/model";
import {ActivatedRoute, Router} from "@angular/router";
import {PermissionsService, RolesService} from "@satipasala/services";
import {Observable} from "rxjs";

@Component({
  selector: 'admin-role-permission-info',
  templateUrl: './role-permission-info.component.html',
  styleUrls: ['./role-permission-info.component.scss']
})
export class RolePermissionInfoComponent implements OnInit {

  allowedPermissions: Array<Permission> = [];
  permissions: Observable<Permission[]>;
  roleId: string;
  role = <Role> {};
  submitted: boolean = false;
  success: boolean = false;

  constructor(protected router: Router, protected route: ActivatedRoute,
              protected rolesService: RolesService, protected permissionsService: PermissionsService) {
  }

  ngOnInit() {
    this.submitted = false;
    this.success = false;
    this.permissions = this.permissionsService.getAll();
    this.route.params.subscribe(params => {
      if (params.roleId) {
        this.roleId = params.roleId;
        this.rolesService.get(this.roleId).subscribe(roleDoc => {
          this.role = roleDoc;
          this.allowedPermissions = this.role.allowedPermissions;
        })
      } else {
        // TODO:
      }
    });
  }

  /**
   * This method will iterate through the users allowedPermission array to check weather a given permission ( from all permissions list)
   * is enabled or not. One purpose of this function is to display users permissions in a window where allowed permissions are
   * marked against all permissions
   * @param {string} permissionToCheck
   * @returns {boolean}
   */
  public isPermissionEnabled(permissionToCheck: string): boolean {
    if (permissionToCheck == null) {
      return false;
    }

    for (let permission of this.allowedPermissions) {
      if (permission.name === permissionToCheck) {
        return true;
      }
    }
    return false;
  }

  addRemovePermission(permission: Permission, $checked) {
    this.submitted = false;
    if ($checked) {
      this.role.allowedPermissions.push(permission);
      console.log("Size :" + this.role.allowedPermissions.length);
    } else {
      console.log("Remove permission " + permission)
      let index: number = this.role.allowedPermissions.indexOf(permission);
      console.log('Before remove ' + this.role.allowedPermissions.length);
      this.role.allowedPermissions.splice(index, 1);
      console.log('After remove ' + this.role.allowedPermissions.length);
    }
  }

  savePermission() {
    this.submitted = true;
    this.rolesService.update(this.roleId, this.role).then(() => {
      console.log("Saved");
      this.success = true;
    }).catch(err => {
      console.error(err);
      this.success = false;
    });
  }
}
