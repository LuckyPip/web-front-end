import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Host, Role} from "@satipasala/model";
import {
  AUTH_MANAGEMENT_ROUTE,
  AUTH_MANAGEMENT_ROUTE_PERMISSIONS,
  AUTH_MANAGEMENT_ROUTE_ROLES, AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_RELATIVE
} from "../../../../app-routs";

@Component({
  selector: 'admin-role-sub-menu',
  templateUrl: './role-sub-menu.component.html',
  styleUrls: ['./role-sub-menu.component.scss']
})
export class RoleSubMenuComponent implements OnInit {

  @Input() role:Role;
  constructor(private router: Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
  }

  editPermissions() {
    this.router.navigate([this.role.id + "/"+AUTH_MANAGEMENT_ROUTE_PERMISSIONS],{relativeTo:this.activatedRoute});/*,relativeTo: this.activatedRoute*/
    // this.router.navigateByUrl(AUTH_MANAGEMENT_ROUTE_ROLES + "/" + this.role.id);
  }

  addRole() {
    this.router.navigate([AUTH_MANAGEMENT_ROUTE_ROLES_ADD_ROLE_RELATIVE],{relativeTo:this.activatedRoute});/*,relativeTo: this.activatedRoute*/
    // this.router.navigateByUrl("auth/roles/add");
  }
}
