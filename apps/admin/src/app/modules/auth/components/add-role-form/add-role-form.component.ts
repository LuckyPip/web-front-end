import { Component, OnInit } from '@angular/core';
import {PermissionsService, RolesService} from "@satipasala/services";
import {Observable} from "rxjs";
import {Permission, Role} from "@satipasala/model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'admin-add-role-form',
  templateUrl: './add-role-form.component.html',
  styleUrls: ['./add-role-form.component.scss']
})
export class AddRoleFormComponent implements OnInit {
  roleForm: FormGroup;
  permissions: Observable<Permission[]>;
  role = <Role> {};
  id: string;
  name: string;
  allowedPermissions: Array<Permission> = [];
  formTitle: string = "Create New Role";
  submitted: boolean = false;
  success: boolean = false;

  constructor(protected rolesService: RolesService, protected permissionsService: PermissionsService, private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.roleForm = this.formBuilder.group({
      roleId : ['', Validators.required ],
      name : ['', Validators.required ]
    });

    this.permissions = this.permissionsService.getAll();
  }

  addRemovePermission(permission: Permission, $checked){
    if($checked){
      this.allowedPermissions.push(permission);
    } else {
      let index: number = this.allowedPermissions.indexOf(permission);
      this.allowedPermissions.splice(index, 1);
    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.roleForm.invalid) {
      return;
    }

    this.role.id = this.roleForm.get("roleId").value;
    this.role.id = this.role.id.toUpperCase();
    this.role.name = this.roleForm.get("name").value;
    this.role.allowedPermissions = this.allowedPermissions;
    this.role.isActive = true;
    this.role.createdAt = new Date();
    this.role.updatedAt = new Date();
    this.rolesService.setDoc(this.role.id,this.role).then(()=>{
      console.log("Saved");
      this.success = true;
    }).catch(err =>{
      console.error(err);
      this.success = false;
    });
  }

  reset(){
    this.roleForm.reset();
    this.submitted = false;
  }
}
