import {AfterViewInit, Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {COURSE_MANAGEMENT_ROUTE} from 'apps/admin/src/app/app-routs';
import {ActivitiesService} from "@satipasala/services";


@Component({
  selector: 'admin-activity-page',
  templateUrl: './activities-page.component.html',
  styleUrls: ['./activities-page.component.css']
})

export class ActivitiesPage implements AfterViewInit {


  constructor(private router: Router, private route: ActivatedRoute, private activityService: ActivitiesService) {
  }

  gotoCoursePage() {
    this.router.navigate([COURSE_MANAGEMENT_ROUTE]);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {

    });

  }

  ngAfterViewInit(): void {
  }
}
