import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActivityListComponent} from './activity-list.component';
import {MaterialModule} from "../../../../imports/material.module";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {ActivatedRoute, Params} from "@angular/router";
import {of} from "rxjs";
import {AngularFirestore} from "@angular/fire/firestore";
import {MockFireStore} from "@satipasala/testing";
import {ActivitiesService} from "@satipasala/services";
import {ActivitySubMenuComponent} from "../activity-sub-menu/activity-sub-menu.component";

describe('ActivityListComponent', () => {
  let component: ActivityListComponent;
  let fixture: ComponentFixture<ActivityListComponent>;
  let params:Params = {id: 1}
  const fakeActivatedRoute = {
    params: of(params)
  } as ActivatedRoute;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[ActivitiesService,{provide: ActivatedRoute, useValue: fakeActivatedRoute},{provide: AngularFirestore, useValue: MockFireStore}],
      declarations: [ActivityListComponent, ActivitySubMenuComponent],
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        RouterTestingModule.withRoutes([])]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
