import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {Activity} from "@satipasala/model";
import {AfterViewInit} from "@angular/core";
import {FirebaseDataSource} from "@satipasala/base";
import {ActivitiesService, CoursesService} from "@satipasala/services";


/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ActivityDataSource extends FirebaseDataSource<Activity> implements AfterViewInit {

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public coursesService: ActivitiesService) {
    super(paginator, sort, coursesService);
  }

  loadAttachedCourses(){

  }
}
