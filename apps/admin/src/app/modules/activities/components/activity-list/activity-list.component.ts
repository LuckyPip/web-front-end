import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {ActivatedRoute} from "@angular/router";
import {ActivitiesService} from "@satipasala/services";
import {ActivityDataSource} from "./activity-list-datasource";
@Component({
  selector: 'admin-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})


export class ActivityListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: ActivityDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','description','pointsPossible', 'isGradable', 'isActive','edit'];
  constructor( private route:ActivatedRoute,private activitiesService:ActivitiesService) {}

  ngOnInit() {
    this.dataSource = new ActivityDataSource(this.paginator, this.sort, this.activitiesService);
  }

  ngAfterViewInit(): void {
    if (this.dataSource) {
      this.dataSource.ngAfterViewInit();
    }
  }

  loadMore(event: PageEvent) {
    if (this.dataSource) {
      this.dataSource.loadMore(event);
    }
  }
}
