import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Activity } from '@satipasala/model';
import * as APP_ROUTES from "../../../../app-routs";
import { ActivitiesService } from '@satipasala/services';

@Component({
  selector: 'admin-activity-form',
  templateUrl: './activity-form.component.html',
  styleUrls: ['./activity-form.component.css']
})


export class ActivityFormComponent implements OnInit{
    
    public formTitle: string;
    public submitBtnText: string;
    public CancelBtnText: string;
    public activityId: string;
    public activity : Activity;
    activityForm: FormGroup;


  constructor(private router: Router, private route : ActivatedRoute, private formBuilder: FormBuilder, private activitiesService : ActivitiesService) {}

  ngOnInit() {

    this.activity = <Activity>{};
    this.activityForm = this.formBuilder.group({
                                  name : '',
                                  description : '',
                                  pointsPossible : 0,
                                  isGradable : '',
                                  isActive : '',
                                  createdAt : '',
                                  updatedAt : ''
                                });

    this.route.params.subscribe(params => {
      this.activityId = params.activityId;
      if (params.activityId == 'new') {
        this.formTitle = "Create Activity";
        this.submitBtnText = "Add";
        this.CancelBtnText = "Cancel";
        this.setActivity()(this.activity);
      } else if(params.action == 'info'){
        this.formTitle = "View Activity";
        this.submitBtnText = "Edit";
        this.CancelBtnText = "Back";
        this.activitiesService.getActivity(params.activityId, this.setActivity());
      } else if(params.action == 'edit') {
        this.formTitle = "Edit Activity";
        this.submitBtnText = "Update";
        this.CancelBtnText = "Cancel";
        this.activitiesService.getActivity(params.activityId, this.setActivity());
      }
    });
  }

  setActivity() {
      return activity => {
        if(activity){
        this.activityForm.patchValue(activity);
       } else {
        this.backToActivities();
       }
    }
  }

  backToActivities() {
    console.log(APP_ROUTES.ACTIVITY_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.ACTIVITY_MANAGEMENT_ROUTE);
  }

  addEditActivity() {
    this.activity = this.activityForm.value;
    if(this.activityId == "new") {
      this.activitiesService.add(this.activity);
    } else if (this.formTitle = "View Activity") {
      this.router.navigateByUrl(APP_ROUTES.ACTIVITY_MANAGEMENT_ROUTE + "/" + this.activityId);
    } else {
      this.activitiesService.update(this.activityId, this.activity);
    }
  }
}
