import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ACTIVITY_MANAGEMENT_ROUTE } from "../../../../app-routs";
import { Activity }  from "@satipasala/model";

@Component({
  selector: 'admin-activity-sub-menu',
  templateUrl: './activity-sub-menu.component.html',
  styleUrls: ['./activity-sub-menu.component.scss']
})
export class ActivitySubMenuComponent implements OnInit {

  @Input() activity:Activity;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  viewActivity(){ 
    this.router.navigateByUrl(ACTIVITY_MANAGEMENT_ROUTE + "/" + this.activity.id + "/info");
  }

  edit() {
    this.router.navigateByUrl(ACTIVITY_MANAGEMENT_ROUTE + "/" + this.activity.id + "/edit");
  }
}
