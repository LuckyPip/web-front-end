import {Component, Inject, LOCALE_ID, NgZone} from '@angular/core';
import {Router} from "@angular/router";

import {SwUpdate} from "@angular/service-worker";
import {
  DASHBOARD_OVERVIEW_ROUTE,
  DASHBOARD_ROUTE,
  HOST_MANAGEMENT_EDIT_ROUTE,
  HOST_MANAGEMENT_INFO_ROUTE,
  HOST_MANAGEMENT_ROUTE,
  USERS_ROUTE,
  USERS_MANAGEMENT_ROUTE,
  USERS_REGISTER_ROUTE,
  COURSE_MANAGEMENT_ROUTE,
  FILE_MANAGEMENT_IMG_UPLOAD_ROUTE,
  FILE_MANAGEMENT_ROUTE,
  FILE_MANAGEMENT_PDF_UPLOAD_ROUTE,
  FILE_MANAGEMENT_AUDIO_FILES_ROUTE, ACTIVITY_MANAGEMENT_ROUTE, QUESTIONNAIRE_MANAGEMENT_ROUTE,
  COURSE_MANAGEMENT_ADD_ROUTE,
  AUTH_MANAGEMENT_ROUTE,
  AUTH_MANAGEMENT_ROUTE_ROLES,
  AUTH_MANAGEMENT_ROUTE_PERMISSIONS, AUTH_MANAGEMENT_ROUTE_ROLES_FORM, QUESTION_MANAGEMENT_ROUTE,
  TRANSLATION_ROUTE, GLOSSARY_ROUTE, QUESTION_MANAGEMENT_ACTION_ROUTE
} from "../../../app-routs";

@Component({
  selector: 'admin-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent {
  title = 'admin';
  //Sidenav responsive
  width;
  height;
  mode: string = 'side';
  open = 'true';
  navList: NavList[];

  constructor(public ngZone: NgZone,
              public route: Router,
              private updates: SwUpdate, @Inject(LOCALE_ID) public locale: string) {

    this.navList = [{
      categoryName: 'Auth Management', icon: 'face', dropDown: false, categoryLink: AUTH_MANAGEMENT_ROUTE,
      subCategory:
        [
          {
            subCategoryName: 'Roles',
            subCategoryLink: AUTH_MANAGEMENT_ROUTE_ROLES,
            subCategoryQuery: {title: 'Roles'},
            visable: true
          },
          {
            subCategoryName: 'Permissions',
            subCategoryLink: AUTH_MANAGEMENT_ROUTE_PERMISSIONS,
            subCategoryQuery: {title: 'Permissions'},
            visable: true
          }
        ]
    },
      {
        categoryName: 'Dashboard', icon: 'face', dropDown: false, categoryLink: DASHBOARD_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Overview',
              subCategoryLink: DASHBOARD_OVERVIEW_ROUTE,
              subCategoryQuery: {title: 'Overview'},
              visable: true
            }
          ]
      },
      {
        categoryName: 'User Management', icon: 'face', dropDown: false, categoryLink: USERS_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Create User',
              subCategoryLink: USERS_REGISTER_ROUTE,
              subCategoryQuery: {title: 'Create User'},
              visable: true
            },
            {
              subCategoryName: 'Manage Users',
              subCategoryLink: USERS_MANAGEMENT_ROUTE,
              subCategoryQuery: {title: 'Manage Users'},
              visable: true
            },
            {
              subCategoryName: 'Text Search',
              subCategoryLink: 'search',
              visable: true
            }
          ]
      }, {
        categoryName: 'Host Management', icon: 'home', dropDown: false, categoryLink: HOST_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Add',
              subCategoryLink: 'new',
              subCategoryQuery: {title: 'Overview'},
              visable: true
            },
            {
              subCategoryName: 'Manage',
              subCategoryLink: HOST_MANAGEMENT_INFO_ROUTE,
              subCategoryQuery: {title: 'Login'},
              visable: true
            }
          ]
      }, {
        categoryName: 'Questionnaire Management',
        icon: 'home',
        dropDown: false,
        categoryLink: QUESTIONNAIRE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Manage',
              subCategoryLink: "",
              subCategoryQuery: {title: 'Login'},
              visable: true
            },
            {
              subCategoryName: 'Questions',
              subCategoryLink: QUESTION_MANAGEMENT_ROUTE,
              subCategoryQuery: {title: 'view'},
              visable: true
            }
          ]
      }, {
        categoryName: 'Course Management', icon: 'face', dropDown: false, categoryLink: COURSE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Coureses',
              subCategoryLink: '',
              visable: true
            },
            {
              subCategoryName: 'Add', subCategoryLink: 'new',
              visable: true
            }
          ]
      }, {
        categoryName: 'Activity Management', icon: 'face', dropDown: false, categoryLink: ACTIVITY_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Add',
              subCategoryLink: 'new',
              //subCategoryQuery: {title: 'Overview'},//what does this do?
              visable: true
            },
            {
              subCategoryName: 'Activities',
              subCategoryLink: '',
              visable: true
            },
          ]
      },
      {
        categoryName: 'ssr', icon: 'face', dropDown: false, categoryLink: "",
        subCategory:
          [
            {subCategoryName: 'Page 1', subCategoryLink: '/ssr', subCategoryQuery: {title: 'ssr page'}, visable: true}
          ]
      },
      {
        categoryName: 'Notes', icon: 'question_answer', dropDown: false, categoryLink: "",
        subCategory:
          [
            {subCategoryName: 'personal notes', subCategoryLink: '/notes', visable: true,}
          ]
      },
      {
        categoryName: 'File Management', icon: 'work', dropDown: false, categoryLink: FILE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {subCategoryName: 'Assignments', subCategoryLink: '/uploads', visable: true},
            {subCategoryName: 'image upload', subCategoryLink: FILE_MANAGEMENT_IMG_UPLOAD_ROUTE, visable: true},
            {subCategoryName: 'pdf upload', subCategoryLink: FILE_MANAGEMENT_PDF_UPLOAD_ROUTE, visable: true},
            {subCategoryName: 'Audio Files', subCategoryLink: FILE_MANAGEMENT_AUDIO_FILES_ROUTE, visable: true}
          ]
      },
      {
        categoryName: 'Translations', icon: 'g_translate', dropDown: false, categoryLink: TRANSLATION_ROUTE,
        subCategory:
          [
            {subCategoryName: 'Glossary', subCategoryLink: GLOSSARY_ROUTE, visable: true}
          ]
      },
    ];
    this.changeMode();
    window.onresize = (e) => {
      ngZone.run(() => {
        this.changeMode();
      });
    };
  }

  /**
   * reload service worker cached resources when there is an update
   */
  reloeadUpdates() {
    this.updates.available.subscribe(() => {
      this.updates.activateUpdate().then(() => window.location.reload())
    })
  }

  ngOnInit() {
    this.reloeadUpdates();
  }

  changeMode() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    if (this.width <= 800) {
      this.mode = 'over';
      this.open = 'false';
    }
    if (this.width > 800) {
      this.mode = 'side';
      this.open = 'true';
    }
  }
}


export class NavList {
  categoryName: string;
  icon: string;
  dropDown: boolean;
  subCategory: NavListItem[];
  categoryLink: string;

  constructor(_categoryName: string, _icon: string, _dropDown: boolean, categoryLink: string = "", _subCategory: NavListItem[]) {
    this.categoryName = _categoryName;
    this.icon = _icon;
    this.dropDown = _dropDown;
    this.subCategory = _subCategory;
    this.categoryLink = categoryLink;
  }
}

export class NavListItem {
  subCategoryName: string;
  subCategoryLink: string;
  subCategoryQuery?: any;
  visable: boolean;
}
