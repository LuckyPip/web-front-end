import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileUploader} from 'ng2-file-upload';
import {UploadItem} from '@satipasala/model';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  @Input() fileTypeData: string | undefined;
  @Input() multiple: boolean = true;
  @Input() loc: string = 'doc';

  uploadForm: FormGroup;
  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  });
  title = 'Angular File Upload';
  uploads: UploadItem[];
  startUpload = false;

  /**
   * Constructor of generic file upload component
   * @param fb
   */
  constructor(private fb: FormBuilder) { }



  /**
   * Prepares selected files before processing. Removes invalid file types from uploader queue and
   * construmap(cts UploadItem with data URL for viewing
   * @param event
   */
  preview(event) {
    this.startUpload = false;
    const fileItems = this.uploader.queue;
    this.uploader.queue = fileItems.filter(t =>
      t._file.type.indexOf(this.fileTypeData.replace('*', '')) !== -1
    );
    if(!this.multiple){
      this.uploader.queue = this.uploader.queue.reverse().slice(0, 1);
    }
    this.uploads = [];
    for (const item of this.uploader.queue) {
      const reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item._file);
      reader.onload = () => {
        // console.log(reader.result);
        this.uploads.push(new UploadItem(item, reader.result));
      };
    }
  }

  /**
   * Removes a selected file before/after uploading
   * @param item FileItem object of the selected row to be deleted
   */
  deleteRow(item) {
    for (let i = 0; i < this.uploads.length; ++i) {
      if (this.uploads[i].item === item) {
         this.uploads.splice(i, 1);
         this.uploader.removeFromQueue(item);
      }
    }
  }

  /**
   * Enables upload button on selected file count
   */
  noneSelected() {
    return this.uploader.queue.length === 0;
  }

  /**
   * Determines whether to display preview when media files are present
   */
  isMediaType(){
    return this.fileTypeData !== undefined && this.fileTypeData.indexOf('image') !== -1;
  }

  /**
   * init method of file-upload component
   */
  ngOnInit() {
    this.startUpload = false;
    this.uploadForm = this.fb.group({
      document: [null, null],
      type:  [null, Validators.compose([Validators.required])]
    });
  }

  /**
   * Initiate file upload task. starUpload flag notifies the initiation of
   * upload-task component, which carries out the actual uploading task
   */
  putFiles(){
    for (const item of this.uploader.queue) {
      const file = item._file;
      if (file.size > 5242880) {
        alert('Each File should be less than 5MB of size.');
        return;
      }
    }
    this.startUpload = true;
  }
}


