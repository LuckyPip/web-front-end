import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UploadPageComponent} from "./upload-page/upload-page.component";
import {
  FILE_MANAGEMENT_PDF_UPLOAD_ROUTE,
  FILE_MANAGEMENT_IMG_UPLOAD_ROUTE,
  FILE_MANAGEMENT_AUDIO_FILES_ROUTE
} from "../../app-routs";
import {CustomUploadComponent} from "./custom-upload/custom-upload.component";

const routes: Routes = [
  {path: FILE_MANAGEMENT_IMG_UPLOAD_ROUTE, component: CustomUploadComponent, data:{ type: 'image/*', location: 'img'}},
  {path: FILE_MANAGEMENT_PDF_UPLOAD_ROUTE, component: CustomUploadComponent, data:{ type: 'application/pdf', location: 'pdf'}},
  {path: 'uploadAudio', component: CustomUploadComponent, data: { type: 'audio/*'}},
  {path: 'uploads', component: UploadPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadsRoutingModule {
}
