import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from "@satipasala/services";
import {Permission, Role, User} from "@satipasala/model";

type UserFields = 'firstName' | 'dob' | 'phoneNumber' | 'email' | 'password' | 'city' | 'organization' | 'userRole' | 'preferredMedium';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'admin-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.scss']
})
export class UserRegistrationFormComponent implements OnInit {

  userForm: FormGroup;
  formErrors: FormErrors = {
    'firstName': '',
    'dob': '',
    'phoneNumber': '',
    'email': '',
    'password': '',
    'city': '',
    'organization': '',
    'userRole': '',
    'preferredMedium': ''
};
  validationMessages = {
    'firstName': {
      'required': 'First Name is required.',
      'maxlength': 'First Name cannot be more than 40 characters long.'
    },
    'dob': {
      'required': 'Date of Birth is required.',
    },
    'phoneNumber': {
      'required': 'Phone Number is required.',
    },
    'email': {
      'required': 'User Role is required.',
      'email': 'Email must be a valid email.'
    },
    'password': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'confirmPassword': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'city': {
      'required': 'City is required.'
    },
    'organization': {
      'required': 'Organization is required.'
    },
    'role': {
      'required': 'Role is required.'
    },
    'preferredMedium': {
      'required': 'Preferred Medium is required.'
    }
  };

  // TODO: Remove hard-coded user roles and retrieve user roles from Firestore
  userRoles: Role[] = [
    {
      id: "SYSTEMADMIN",
      name: "System Administrator",
    },
    {
      id: "READONLY",
      name: "Read only access to the system",
    },
  ];

  preferredMediums: string[] = [
    "English",
    "Sinhala",
    "Tamil"
  ];

  constructor(private fb: FormBuilder, private auth: AuthService) {
  }

  ngOnInit() {
    this.buildForm();
  }

  onSubmit() {

    const userData: User = {
      firstName : this.userForm.value['firstName'],
      dob : this.userForm.value['dob'],
      phoneNumber : this.userForm.value['phoneNumber'],
      email : this.userForm.value['email'],
      password : this.userForm.value['password'],

      city : this.userForm.value['city'],
      organization : this.userForm.value['organization'],

      userRole : this.userForm.value['userRole'],
      preferredMedium : this.userForm.value['preferredMedium']
    };

    this.auth.createFirebaseUser(this.userForm.value['email'], this.userForm.value['password']);
    this.auth.createUserByAdmin(userData).then(() =>
      window.location.reload()
    );

    this.auth.registerUserByAdmin(
      this.userForm.value['firstName'],
      this.userForm.value['lastName'],
      this.userForm.value['displayName'],
      this.userForm.value['userRole'],
      this.userForm.value['email'],
      this.userForm.value['password'],
    ).then(() =>
      // TODO: Handle successful submits
      window.location.reload()
    );
  }

  buildForm() {
    this.userForm = this.fb.group({
      'firstName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'dob': ['', [
        Validators.required
      ]],
      'phoneNumber': ['', [
        Validators.pattern('[0-9]{10}'),
        Validators.required
      ]],
      'userRole': ['', [
        Validators.required,
      ]],
      'email': ['', [
        Validators.required,
        Validators.email,
      ]],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'confirmPassword': ['', [
        Validators.required,
      ]],
      'city': ['', [
        Validators.required,
      ]],
      'organization': ['', [
        Validators.required,
      ]],
      'preferredMedium': ['', [
        Validators.required,
      ]],

      'lastName': [],
      'nic': [],
      'street': [],
      'province': [],
      'country': [],
    });

    this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    if (!this.userForm) {
      return;
    }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field) && (field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
                this.formErrors[field] += `${(messages as { [key: string]: string })[key]} `;
              }
            }
          }
        }
      }
    }
  }
}
