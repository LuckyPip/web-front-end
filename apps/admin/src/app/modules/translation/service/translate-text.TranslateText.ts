export class TranslateText {
  uid:string;
  lang: string;
  model: string;
  translated: string;
  phrase: string;
}
