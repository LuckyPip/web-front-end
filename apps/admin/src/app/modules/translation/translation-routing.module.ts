import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GLOSSARY_ROUTE} from "../../app-routs";
import {TranslationPage} from "./pages/translation-page/translation-page.component";


const routes: Routes = [
  {path: '', redirectTo: GLOSSARY_ROUTE, pathMatch: 'full'},
  {path: GLOSSARY_ROUTE, component: TranslationPage}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TranslationRoutingModule {
}
