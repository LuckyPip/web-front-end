import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslationRoutingModule} from './translation-routing.module';
import {CoreComponentsModule, DynamicFormsModule} from "@satipasala/base";
import {TranslationPage} from './pages/translation-page/translation-page.component';
import {MaterialModule} from "../../imports/material.module";
import {AngularFireModule} from "@angular/fire";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {GlossaryTableComponent} from './components/glossary-table/glossary-table.component';
import {ContextMenuComponent} from './components/context-menu/context-menu.component';


@NgModule({
  declarations: [TranslationPage, GlossaryTableComponent, ContextMenuComponent],
  imports: [
    CommonModule,
    MaterialModule,
    TranslationRoutingModule,
    DynamicFormsModule,
    CoreComponentsModule,
    AngularFireModule,
    AngularFirestoreModule
  ]
})
export class TranslationModule {
}
