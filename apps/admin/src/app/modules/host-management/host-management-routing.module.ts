import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {
  HOST_LOCATIONS_INFO_ROUTE,
  HOST_MANAGEMENT_INFO_ROUTE,
  HOST_LOCATIONS_EDIT_ROUTE,
  HOST_MANAGEMENT_EDIT_ROUTE
} from "../../app-routs";
import {HostManagementPageComponent} from "./pages/host-management-page/host-management-page.component";
import {LocationInfoComponent} from "./components/location-info-component/location-info.component";
import {LocationFormComponent} from './components/location-form-component/location-form.component';
import {HostFormComponent} from './components/host-form/host-form.component';

const routes: Routes = [

  {
    path: HOST_MANAGEMENT_INFO_ROUTE, component: HostManagementPageComponent,

  },
  {path: HOST_LOCATIONS_INFO_ROUTE, component: LocationInfoComponent},
  {path: HOST_LOCATIONS_EDIT_ROUTE, component: LocationFormComponent},
  {path: HOST_MANAGEMENT_EDIT_ROUTE, component: HostFormComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HostManagementRoutingModule {
}
