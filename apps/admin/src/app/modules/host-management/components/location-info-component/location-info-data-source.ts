import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {HostLocation} from "@satipasala/model";
import {AfterViewInit} from "@angular/core";
import {FirebaseDataSource} from "@satipasala/base";
import {HostsService} from "@satipasala/services";
import { QueryFn } from '@angular/fire/firestore';


/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class LocationInfoDataSource extends FirebaseDataSource<HostLocation> implements AfterViewInit {
  hostId: string;
  public static subCollection:string = "locations"

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public hostsService: HostsService, hostId: string) {
    super(paginator, sort, hostsService);
    this.hostId = hostId;
  }

  public getArrayData (documentId: string, arrayField: string, queryFn?: QueryFn) {
    this.collectionService.get(documentId,queryFn).subscribe(
      actions => {
        const data = actions.payload.data();
        this.dataSubject.next(data[arrayField]);
      }
    )
  }

  loadMore(event: PageEvent) {
    this.getArrayData(this.hostId, 'locations', query => query.orderBy("id").startAt(event.pageIndex).limit(event.pageSize))
  }

  ngAfterViewInit(): void {
    this.getArrayData(this.hostId, 'locations');
  }
}
