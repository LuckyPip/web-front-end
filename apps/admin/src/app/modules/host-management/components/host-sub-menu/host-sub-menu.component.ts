import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HOST_LOCATIONS_INFO_ROUTE, HOST_MANAGEMENT_ROUTE} from "../../../../app-routs";
import {Host} from "@satipasala/model";

@Component({
  selector: 'admin-host-sub-menu',
  templateUrl: './host-sub-menu.component.html',
  styleUrls: ['./host-sub-menu.component.scss']
})
export class HostSubMenuComponent implements OnInit {

  @Input() host:Host;
locationsRoute:string = HOST_LOCATIONS_INFO_ROUTE;
  constructor(private router: Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
  }

  edit() {
    this.router.navigateByUrl(HOST_MANAGEMENT_ROUTE + "/" + this.host.id);
  }

  showLocations(){
    //this.router.navigateByUrl(HOST_LOCATIONS_INFO_ROUTE,{queryParams:{"hostId":this.host._id}});
  //  this.router.navigateByUrl("host/location",{queryParams:{"hostId":this.host.id}});

    this.router.navigate([HOST_MANAGEMENT_ROUTE + "/" + this.host.id + "/location"]);/*,relativeTo: this.activatedRoute*/
  }
}
