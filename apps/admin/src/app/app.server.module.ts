import { NgModule } from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
//const routes: Routes = [ { path: 'shell', component:  }];

@NgModule({
  imports: [
    AppModule,
    ServerModule,
   // RouterModule.forRoot(routes),
    ServerTransferStateModule,
    ModuleMapLoaderModule,
    NoopAnimationsModule
  ],
  bootstrap: [AppComponent],
  declarations: [],
})
export class AppServerModule {}
