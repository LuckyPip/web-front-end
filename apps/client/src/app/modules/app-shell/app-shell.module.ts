import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellRoutingModule } from './app-shell-routing.module';
import {CoreModule} from "../core/core.module";
import {MaterialModule} from "../../imports/material.module";
import {AppShellComponent} from "./app-shell.component/app-shell.component";
import {CoreComponentsModule} from "@satipasala/base";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {AngularFireFunctionsModule} from "@angular/fire/functions";
import {BrowserTransferStateModule} from "@angular/platform-browser";
import 'hammerjs';
import {AuthGuard, GuardsModule} from "@satipasala/guards";
import {AuthService, ServicesModule} from "@satipasala/services";
import {ErrorStateMatcher, MAT_LABEL_GLOBAL_OPTIONS, ShowOnDirtyErrorStateMatcher} from "@angular/material";

@NgModule({
  declarations: [AppShellComponent,],
  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    CommonModule,
    CoreModule,
    GuardsModule,
    ServicesModule,
    BrowserTransferStateModule,
    AppShellRoutingModule,
    CoreComponentsModule
  ],
  providers: [AuthService,AuthGuard,
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  bootstrap:[AppShellComponent]

})
export class AppShellModule { }
