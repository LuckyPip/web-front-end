import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "@satipasala/guards";
import {AppShellComponent} from "./app-shell.component/app-shell.component";


const routes: Routes = [
  {
    path: '', component: AppShellComponent,
    children: [

    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppShellRoutingModule {
}
