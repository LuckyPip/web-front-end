import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule, MatIconModule,
  MatInputModule, MatMenuModule, MatPaginatorModule,
  MatRadioModule,
  MatSelectModule, MatSortModule,
  MatTableModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../../imports/material.module";
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {MenuComponent} from './components/menu/menu.component';
import {NotificationComponent} from './components/notification/notification.component';
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthService, ServicesModule} from "@satipasala/services";
import {SearchComponent} from "../search/components/search.component";
import {EventBannerComponent} from "../event/components/event-banner/event-banner.component";


@NgModule({
  providers: [AngularFirestore, AuthService],

  declarations: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent,
    SearchComponent,
    EventBannerComponent
  ],
  imports: [
    ServicesModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MaterialModule,


  ],

  exports: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent,
    SearchComponent,
    EventBannerComponent

  ]
})
export class CoreModule {
}
